**Instructions:**

- Download the code or clone it.
- Include it in Matlab's search path (you can write `addpath(genpath('./aau-t2d-simulator'))` or do it manually).

**To perform a simple quick simulation, run the following code:**

`p = T2Dp; %Construct a patient object p from the class T2Dp`

`%Define three meals at 7 (40g), 12 (100g), and 18 (140g) everyday:`

`p.Meals = p.perday( [7*60 12*60 18*60] , [40 100 140]);`

`p.Simulation; %Simulate the patient`

`X = p.X;`

`time = p.time;`

**To perform closed loop implementations with SMBG measurements, check the following examples:**
- ClosedloopSMBG.m
- ClosedloopSMBGrandompatients.m

_A detailed description of the model can be found in this [paper](https://www.sciencedirect.com/science/article/pii/S1369703X21002461)_
